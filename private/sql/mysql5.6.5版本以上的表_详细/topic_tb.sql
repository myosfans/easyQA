CREATE TABLE `easyqa`.`topic_tb`( 
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
  `topic` CHAR(20) NOT NULL COMMENT '话题',
  `used_times` INT UNSIGNED NOT NULL DEFAULT 1 COMMENT '话题使用次数',
  `add_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '话题添加时间',
  PRIMARY KEY `id_pk` (`id`),
  UNIQUE INDEX `topic_uq` (`topic`),
  INDEX `usedTimes_index` (`used_times`)
) ENGINE=MYISAM COMMENT='话题表' CHARSET=utf8 COLLATE=utf8_general_ci;