<div class="layui-tab layui-tab-brief mb30 mt0">
    <ul class="layui-tab-title">
        <li<?=$active_nav == 'q' ? ' class="layui-this"' : ''?>><a href="/u">我的提问 <cite><?=$article_counts?></cite></a></li>
        <li<?=$active_nav == 'comment' ? ' class="layui-this"' : ''?>><a href="/u/comment">我的回答 <cite><?=$comment_counts?></cite></a></li>
        <li<?=$active_nav == 'msg' ? ' class="layui-this"' : ''?>><a href="/u/msg">我的消息 <cite><?=$msg_to_me_counts?></cite></a></li>
        <li<?=$active_nav == 'avatar' ? ' class="layui-this"' : ''?>><a href="/u/avatar">我的头像</a></li>
        <li<?=$active_nav == 'reset_pwd' ? ' class="layui-this"' : ''?>><a href="/u/reset_pwd">修改密码</a></li>
        <li<?=$active_nav == 'bind' ? ' class="layui-this"' : ''?>><a href="/u/bind">账号绑定</a></li>
        <li<?=$active_nav == 'profile' ? ' class="layui-this"' : ''?>><a href="/u/profile">个人资料</a></li>
        <li><a href="/u/home/<?=$user['id']?>" target="_blank">我的主页</a></li>
    </ul>
</div>