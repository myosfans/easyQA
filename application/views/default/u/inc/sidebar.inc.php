<div class="edge">
    <div class="user-about">
        <a href="" title="修改头像">
            <img class="user-avatar" src="<?=create_avatar_url($user['id'], $user['avatar_ext'])?>">
        </a>
        <p>
            <span style="color:#333"><?=$user['nickname']?></span>
            <?php if ($user['gender'] != 'n'): ?>
                <i class="iconfont icon-<?=$user['gender']?>"></i>
            <?php endif;?>
        </p>
        <p>
            <span>加入时间：<?=date('Y-m-d', strtotime($user['signup_time']))?></span>
        </p>
    </div>
</div>